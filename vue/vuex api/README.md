# Vuex with an API

## Use Case

Vuex populated from an REST API.

## Implementation

Make use of Vuex actions to fetch data from a REST API request and callback to commit Vuex mutations.

A small ORM-like library removed duplicate code from the various store's modules. It has also decoupled the API's request from the store's modules making it easier to change the data source if required.