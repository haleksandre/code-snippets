import * as types from '../mutation-types' // eslint-disable-line import/no-namespace

// Initial State
const state = {
  offices: {
    en: require(`../../../../meta/menus/offices-en.yaml`),
    fr: require(`../../../../meta/menus/offices-fr.yaml`)
  }
}

// Getters
const getters = {
  all: (state, getters, rootState, rootGetters) => state.offices[rootGetters.locale]
}

// Actions
const actions = {}

// Mutations
const mutations = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
