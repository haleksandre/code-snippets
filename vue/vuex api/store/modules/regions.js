import Region from '../../api/region'
import * as types from '../mutation-types' // eslint-disable-line import/no-namespace

// Initial State
const state = {
  region: {},
  regions: [],
  search: []
}

// Getters
const getters = {
  region: state => state.region,
  all: state => state.regions,
  search: state => state.search,
  filtered: state => provinceId => {
    return state.regions.filter(region => region.province_id === provinceId)
  }
}

// Actions
const actions = {
  // Fetch all records
  all: async ({ commit }) => {
    const region = new Region
    const cb = regions => commit(types.ALL_REGIONS, { regions })

    await region.params({ _sort: 'name' })
                .get(cb)
  },
  // Fetch one records by id
  findById: async ({ commit }, id) => {
    const region = new Region
    const cb = region => commit(types.FIND_REGION, { region })

    await region.find(id)
                .get(cb)
  },
  // Fetch one records by slug
  findBySlug: async ({ commit }, slug) => {
    const region = new Region
    const cb = region => commit(types.FIND_REGION, { region: region[0] })

    await region.params({ slug })
                .get(cb)
  },
  // Fetch related province records 
  findByProvince: async ({ commit }, provinceId) => {
    const region = new Region
    const cb = regions => commit(types.SEARCH_REGIONS, { regions })

    await region.with('province', provinceId)
                .get(cb)
  },
}

// Mutations
const mutations = {
  [types.ALL_REGIONS]: (state, { regions }) => {
    state.regions = regions
  },
  [types.SEARCH_REGIONS]: (state, { regions }) => {
    state.search = regions
  },
  [types.FIND_REGION]: (state, { region }) => {
    state.region = region
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
