import * as types from '../mutation-types' // eslint-disable-line import/no-namespace

// Initial State
const state = {
  domains: {
    en: require(`../../../../meta/menus/domains-en.yaml`),
    fr: require(`../../../../meta/menus/domains-fr.yaml`)
  }
}

// Getters
const getters = {
  all: (state, getters, rootState, rootGetters) => state.domains[rootGetters.locale]
}

// Actions
const actions = {}

// Mutations
const mutations = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
