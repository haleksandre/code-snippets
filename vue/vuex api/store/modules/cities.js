import City from '../../api/city'
import * as types from '../mutation-types' // eslint-disable-line import/no-namespace

// Initial State
const state = {
  city: {},
  cities: [],
  search: []
}

// Getters
const getters = {
  city: state => state.city,
  all: state => state.cities,
  search: state => state.search
}

// Actions
const actions = {
  // Fetch all records
  all: async ({ commit }) => {
    const city = new City

    await city.all(cities => commit(types.ALL_CITIES, { cities }))
  },
  // Fetch one records by id
  findById: async ({ commit }, id) => {
    const city = new City
    const cb = city => commit(types.FIND_CITY, { city })

    await city.find(id)
              .get(cb)
  },
  findByRegion: async ({ commit }, regionId) => {
    const city = new City
    const cb = cities => commit(types.SEARCH_CITIES, { cities })

    const params = {
      region_id: regionId
    }

    await city.params(params)
              .get(cb)
  },
  findByRegions: async ({ commit }, regions) => {
    const city = new City
    const cb = cities => commit(types.SEARCH_CITIES, { cities })
    const regionsId = regions.map(region => region.id)

    const params = {
      region_id: regionsId
    }

    await city.params(params)
              .get(cb)
  }
}

// Mutations
const mutations = {
  [types.ALL_CITIES]: (state, { cities }) => {
    state.cities = cities
  },
  [types.SEARCH_CITIES]: (state, { cities }) => {
    state.search = cities
  },
  [types.FIND_CITY]: (state, { city }) => {
    state.city = city
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
