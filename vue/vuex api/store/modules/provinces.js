import Province from '../../api/province'
import * as types from '../mutation-types' // eslint-disable-line import/no-namespace

// Initial State
const state = {
  province: {},
  provinces: [],
}

// Getters
const getters = {
  province: state => state.province,
  all: state => state.provinces,
  hasRegions: state => {
    return state.provinces.filter(province => province.regions.length)
  }
}

// Actions
const actions = {
  // Fetch all records
  all: async ({ commit }) => {
    const province = new Province
    const cb = provinces => commit(types.ALL_PROVINCES, { provinces })

    await province.params({ _sort: 'name' })
                  .get(cb)
  },
  // Fetch all records with embeded regions
  withRegions: async ({ commit }) => {
    const province = new Province
    const cb = provinces => commit(types.ALL_PROVINCES, { provinces })

    const params = {
      _embed: 'regions'
    }

    await province.params(params)
                  .get(cb)
  },
  // Fetch one records by id
  findById: async ({ commit }, id) => {
    const province = new Province
    const cb = province => commit(types.FIND_PROVINCE, { province })

    await province.find(id)
                  .get(cb)
  },
  // Fetch one records by slug
  findBySlug: async ({ commit }, slug) => {
    const province = new Province
    const cb = province => commit(types.FIND_PROVINCE, { province: province[0] })

    await province.params({ slug })
                  .get(cb)
  }
}

// Mutations
const mutations = {
  [types.ALL_PROVINCES]: (state, { provinces }) => {
    state.provinces = provinces
  },
  [types.FIND_PROVINCE]: (state, { province }) => {
    state.province = province
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
