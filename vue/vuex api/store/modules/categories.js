import Category from '../../api/category'
import * as types from '../mutation-types' // eslint-disable-line import/no-namespace

// Initial State
const state = {
  category: {},
  categories: []
}

// Getters
const getters = {
  category: state => state.category,
  all: state => state.categories,
  current: (state, getters) => slug => (
    state.categories.find((category) => slug === category.slug)
  )
}

// Actions
const actions = {
  // Fetch all records
  all: async ({ commit }) => {
    const category = new Category
    const cb = categories => commit(types.ALL_CATEGORIES, { categories })

    await category.params({ _sort: 'name' })
                  .get(cb)
  },
  // Fetch one records by id
  findById: async ({ commit }, id) => {
    const category = new Category
    const cb = category => commit(types.FIND_CATEGORY, { category })

    await category.find(id)
                  .get(cb)
  }
}

// Mutations
const mutations = {
  [types.ALL_CATEGORIES]: (state, { categories }) => {
    state.categories = categories
  },
  [types.FIND_CATEGORY]: (state, { category }) => {
    state.category = category
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
