import Posting from '../../api/posting'
import * as types from '../mutation-types' // eslint-disable-line import/no-namespace
import { defaultsDeep, pickBy } from 'lodash'
import parseLink from 'parse-link-header'

// Initial State
const state = {
  posting: {},
  postings: [],
  featured: [],
  table: {
    total: 0,
    link: {
      first: null,
      prev: null,
      next: null,
      last: null
    }
  }
}

// Getters
const getters = {
  posting: state => state.posting,
  all: state => state.postings,
  featured: state => state.featured,
  table: state => state.table
}

// Actions
const actions = {
  // Fetch all records
  all: async ({ commit }) => {
    const posting = new Posting

    await posting.all(postings => commit(types.ALL_POSTINGS, { postings }))
  },
  // Fetch featured posting records
  featured: async ({ commit }, params) => {
    const posting = new Posting
    const cb = postings => commit(types.FEATURED_POSTINGS, { postings })

    params = pickBy({
      is_featured: true,
      ...params
    })

    await posting.params(params)
                 .get(cb)
  },
  // Fetch records by params
  listings: async ({ commit }, params) => {
    const posting = new Posting
    const cb = (postings, headers) => commit(types.LISTINGS_POSTINGS, { postings, headers })

    params = pickBy({
      is_internal: false,
      _expand: 'city',
      _limit: 20,
      ...params
    })

    await posting.params(params)
                 .get(cb)
  },
  // Fetch internal posting records
  internal: async ({ commit }) => {
    const posting = new Posting
    const cb = postings => commit(types.INTERNAL_POSTINGS, { postings })

    await posting.params({ is_internal: true })
                 .get(cb)
  },
  // Fetch one records by id
  findById: async ({ commit }, id) => {
    const posting = new Posting
    const cb = posting => commit(types.FIND_POSTING, { posting })

    await posting.find(id)
                 .params({ _expand: ['category', 'city'] })
                 .get(cb)
  }
}

// Mutations
const mutations = {
  [types.ALL_POSTINGS]: (state, { postings }) => {
    state.postings = postings
  },
  [types.FEATURED_POSTINGS]: (state, { postings }) => {
    state.featured = postings
  },
  [types.LISTINGS_POSTINGS]: (state, { postings, headers }) => {
    state.table.total = parseInt(headers['x-total-count'])
    state.table.link = defaultsDeep(parseLink(headers.link), state.table.link)
    state.postings = postings
  },
  [types.INTERNAL_POSTINGS]: (state, { postings }) => {
    state.postings = postings
  },
  [types.FIND_POSTING]: (state, { posting }) => {
    state.posting = posting
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
