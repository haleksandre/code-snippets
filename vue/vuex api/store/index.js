import Vue from 'vue'
import Vuex from 'vuex'
import { locale } from '../config/'

// Modules
import categories from './modules/categories'
import cities from './modules/cities'
import domains from './modules/domains'
import offices from './modules/offices'
import postings from './modules/postings'
import provinces from './modules/provinces'
import regions from './modules/regions'

// Install Vuex
Vue.use(Vuex)

// Global State
const state = {
  locale
}

// Global Getters
const getters = {
  locale: state => state.locale.current,
  locales: state => state.locale.locales,
}

// Export Vuex Store
export default new Vuex.Store({
  state,
  getters,
  modules: {
    categories,
    domains,
    postings,
    provinces,
    offices,
    regions,
    cities,
  },
  strict: process.env.NODE_ENV !== 'production',
})
