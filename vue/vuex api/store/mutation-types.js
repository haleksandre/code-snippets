// Domains
export const ALL_DOMAINS = 'ALL_DOMAINS'

// Categories
export const ALL_CATEGORIES = 'ALL_CATEGORIES'
export const FIND_CATEGORY = 'FIND_CATEGORY'

// Postings
export const ALL_POSTINGS = 'ALL_POSTINGS'
export const FEATURED_POSTINGS = 'FEATURED_POSTINGS'
export const INTERNAL_POSTINGS = 'INTERNAL_POSTINGS'
export const LISTINGS_POSTINGS = 'LISTINGS_POSTINGS'
export const FIND_POSTING = 'FIND_POSTING'

// Provinces
export const ALL_PROVINCES = 'ALL_PROVINCES'
export const FIND_PROVINCE = 'FIND_PROVINCE'

// Regions
export const ALL_REGIONS = 'ALL_REGIONS'
export const FIND_REGION = 'FIND_REGION'
export const SEARCH_REGIONS = 'SEARCH_REGIONS'

// Cities
export const ALL_CITIES = 'ALL_CITIES'
export const SEARCH_CITIES = 'SEARCH_CITIES'
export const FIND_CITY = 'FIND_CITY'
