import Base from './base'

export default class Province extends Base {
  /**
   * Create an instance of Province
   *
   * @return {Province} A new instance of Province
   */
  constructor () {
    super('provinces')
  }
}
