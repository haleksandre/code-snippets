import Base from './base'

export default class City extends Base {
  /**
   * Create an instance of City
   *
   * @return {City} A new instance of City
   */
  constructor () {
    super('cities')
  }
}
