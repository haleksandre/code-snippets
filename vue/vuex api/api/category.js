import Base from './base'

export default class Category extends Base {
  /**
   * Create an instance of Category
   *
   * @return {Category} A new instance of Category
   */
   constructor () {
    super('categories')
  }
}
