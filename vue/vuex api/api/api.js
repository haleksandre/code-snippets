import axios from 'axios'
import { isFunction, defaultsDeep } from 'lodash'
import store from '../store/'
import config from '../config/axios'

// Create an axios client
const client = axios.create(config)

/**
 * The default parameters to be pass 
 * to the axios request.
 *
 * @param {Object|Function} config The request configuration
 * @param {Function} cb The request callback
 * @return {Array} The axios default parameters and the callback
 */
const _defaults = (config, cb) => {
  if (isFunction(config)) {
    cb = config
    config = {}
  }

  config = defaultsDeep(config, {
    params: {
      locale: store.getters.locale,
    }
  })

  return [config, cb]
}

/**
 * Create an axios request
 *
 * @param {String} request The request type
 * @param {String} url The request url
 * @param {Object|Function} config The request configuration
 * @param {Function} cb The request callback
 * @return {Promise} The axios request promise
 */
const request = async (request, url, config, cb) => {
  [config, cb] = _defaults(config, cb)

  try {
    const response = await client[request](url, config)
    cb(response.data, response.headers)
  } catch (error) {
    console.log(error) // eslint-disable-line no-console
  }
}

export default class Api {
  /**
   * Create a GET request
   *
   * @param {String} url The request url
   * @param {Object|Function} config The request configuration
   * @param {Function} cb The request callback
   * @return {Promise} The axios request promise
   */
  static async get (url, config, cb) {
    await request('get', url, config, cb)
  }

  /**
   * Create a POST request
   *
   * @param {String} url The request url
   * @param {Object|Function} config The request configuration
   * @param {Function} cb The request callback
   * @return {Promise} The axios request promise
   */
  static async post (url, config, cb) {
    await request('post', url, config, cb)
  }

  /**
   * Create a PATCH request
   *
   * @param {String} url The request url
   * @param {Object|Function} config The request configuration
   * @param {Function} cb The request callback
   * @return {Promise} The axios request promise
   */
  static async patch (url, config, cb) {
    await request('patch', url, config, cb)
  }

  /**
   * Create a PUT request
   *
   * @param {String} url The request url
   * @param {Object|Function} config The request configuration
   * @param {Function} cb The request callback
   * @return {Promise} The axios request promise
   */
  static async put (url, config, cb) {
    await request('put', url, config, cb)
  }

  /**
   * Create a DELETE request
   *
   * @param {String} url The request url
   * @param {Object|Function} config The request configuration
   * @param {Function} cb The request callback
   * @return {Promise} The axios request promise
   */
  static async delete (url, config, cb) {
    await request('delete', url, config, cb)
  }
}
