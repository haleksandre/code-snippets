import Api from './api'
import { merge } from 'lodash'

export default class Base {
  /**
   * Create an instance of Category
   *
   * @param {String} resource The resource name
   * @return {Base} A new instance of Base
   */
  constructor (resource) {
    this.url = { resource }
    this.config = {}
  }

  /**
   * Format the url for the request
   *
   * @return {String} The formatted url
   */
  getUrl () {
    let url = this.url.resource

    if (this.url.id) {
      url += `/${this.url.id}`
    }

    if (this.url.nested) {
      url += `/${this.url.nested}`
    }

    return url
  }

  /**
   * Fetches all the record of selected resource
   *
   * @param {Function} cb The callback function
   * @return {Promise} The axios request promise
   */
  async all (cb) {
    const url = this.url.resource

    return await Api.get(url, cb)
  }

  /**
   * Append the id of a resource to the current url
   *
   * @param {String|Number} id The resource id
   * @return {Base} The updated instance of Base
   */
  find (id) {
    this.url.id = id

    return this
  }

  /**
   * Append a nested resource to the current url
   *
   * @param {String} resource The resource for the api request
   * @param {String|Number} id The resource id 
   * @return {Base} The updated instance of Base
   */
   with (resource, id) {
    if (id) {
      this.url.nested = this.url.resource

      this.url = merge(this.url, { resource, id })
    } else {
      this.url.nested = resource
    }

    return this
  }

  /**
   * Set the axios client configuration
   *
   * @param {Object} params The axios configuration
   * @return {Base} The updated instance of Base
   */
  params (params) {
    this.config = params

    return this
  }

  /**
   * Fetches records with the customize url and configuration
   *
   * @param {Function} cb The callback function
   * @return {Promise} The axios request promise
   */
  async get (cb) {
    const url = this.getUrl()
    const params = this.config

    return await Api.get(url, { params }, cb)
  }
}
