import Base from './base'

export default class Posting extends Base {
  /**
   * Create an instance of Posting
   *
   * @return {Posting} A new instance of Posting
   */
  constructor () {
    super('postings')
  }
}
