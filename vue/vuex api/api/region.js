import Base from './base'

export default class Region extends Base {
  /**
   * Create an instance of Region
   *
   * @return {Region} A new instance of Region
   */
  constructor () {
    super('regions')
  }
}
