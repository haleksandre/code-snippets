# Form Data Transformer

## Use Case

OctoberCMS (Laravel-based CMS) website need to communicate with an unconventional API that had little to no convention (Resource and parameters' naming, URLs, etc).

## Implementation

A minimal transformer class to prevent polluting the code of the website with the various naming convention of the API.

This class responsibility is to convert the website form data, which uses English naming convention, to match the API's parameters various names as well as to match Guzzle multipart / JSON array convention.
