<?php namespace Haleks\Forms\Transformers;

use Haleks\Forms\Transformers\Transformer;

class MultipartTransformer extends Transformer
{
    /**
     * Transform the data keys with the
     * the given keys.
     *
     * @param  Array  $data
     * @param  Array  $transform
     * @return Array
     */
    public function transform($data, $transform = [])
    {
        if (empty($transform)) {
            $transform = $this->transform;
        }

        $transform = array_dot($transform);

        $transformedData = collect($transform)
            ->flatMap(function ($currentKey, $newKey) use ($data) {
                if (array_has($data, $currentKey)) {
                    return [
                        [
                            'name' => preg_replace('/[.]([^.&=]+)/', '[$1]', $newKey),
                            'contents' => array_get($data, $currentKey),
                        ]
                    ];
                }
            })
            ->toArray();

        return ['multipart' => $transformedData];
    }
}
