<?php namespace Haleks\Forms\Transformers;

use Haleks\Forms\Transformers\JsonTransformer;
use Haleks\Forms\Transformers\MultipartTransformer;

class ApplicationTransformer
{
    protected $transform = [
        'reference' => 'reference',
        'nom' => 'lastname',
        'prenom' => 'firstname',
        'email' => 'email',
        'telephone' => 'phone',
        'address' => 'address',
        'category' => 'category',
        'city' => 'city',
        'document' => 'attachment',
        'educations' => [
            [
                'title' => 'educations.0.title',
                'institution' => 'educations.0.institution',
                'diploma' => 'educations.0.diploma',
            ],
            [
                'title' => 'educations.1.title',
                'institution' => 'educations.1.institution',
                'diploma' => 'educations.1.diploma',
            ],
            [
                'title' => 'educations.2.title',
                'institution' => 'educations.2.institution',
                'diploma' => 'educations.2.diploma',
            ],
            [
                'title' => 'educations.3.title',
                'institution' => 'educations.3.institution',
                'diploma' => 'educations.3.diploma',
            ],
        ],
        'positions' => [
            [
                'title' => 'positions.0.title',
                'start' => 'positions.0.start',
                'end' => 'positions.0.end',
            ],
            [
                'title' => 'positions.1.title',
                'start' => 'positions.1.start',
                'end' => 'positions.1.end',
            ],
            [
                'title' => 'positions.2.title',
                'start' => 'positions.2.start',
                'end' => 'positions.2.end',
            ],
            [
                'title' => 'positions.3.title',
                'start' => 'positions.3.start',
                'end' => 'positions.3.end',
            ],
        ],
        'languages' => [
            [
                'name' => 'languages.0.name',
                'level' => 'languages.0.level',
            ],
            [
                'name' => 'languages.1.name',
                'level' => 'languages.1.level',
            ],
            [
                'name' => 'languages.2.name',
                'level' => 'languages.2.level',
            ],
            [
                'name' => 'languages.3.name',
                'level' => 'languages.3.level',
            ],
            [
                'name' => 'languages.4.name',
                'level' => 'languages.4.level',
            ],
            [
                'name' => 'languages.5.name',
                'level' => 'languages.5.level',
            ],
        ],
        'skills' => 'skills',
        'interests' => 'interests',
        'agreement' => 'newsletter',
    ];

    /**
     * Returns the transformed data to the given type format.
     *
     * @param  Array  $data
     * @param  String  $type
     * @return Array
     */
    public function transform($data, $type = 'multipart')
    {
        $transformer = __NAMESPACE__.'\\'.ucfirst($type).'Transformer';

        if (!class_exists($transformer)) {
            return $data;
        }

        return (new $transformer($this->transform))->transform($data);
    }
}
