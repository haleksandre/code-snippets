<?php namespace Haleks\Forms\Transformers;

use Haleks\Forms\Transformers\Transformer;

class JsonTransformer extends Transformer
{
    /**
     * Transform the data keys with the
     * the given keys.
     *
     * @param  Array  $data
     * @param  Array  $transform
     * @return Array
     */
    public function transform($data, $transform = [])
    {
        if (empty($transform)) {
            $transform = $this->transform;
        }

        $transform = array_dot($transform);

        $transformedData = collect($transform)
            ->map(function ($currentKey, $newKey) use ($data) {
                return data_get($data, $currentKey, null);
            })
            ->filter()
            ->toArray();

        return ['json' => array_dig($transformedData)];
    }
}
