<?php namespace Haleks\Forms\Controllers;

use Config;
use Validator;
use Illuminate\Http\Request;
use RainLab\Translate\Models\Message;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Cms\Classes\CmsController as Controller;
use Haleks\Forms\Transformers\ApplicationTransformer;

class ApplicationController extends Controller
{
    protected $translation;

    public function __construct()
    {
        parent::__construct();
        $this->translation = new Message;
    }

    /**
     * Validate form and make a post request
     * for job posting application.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function apply(Request $request)
    {
        $validator = $this->validate($request, [
            'id' => 'required',
            'reference' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        return $this->postApi($request, 'application');
    }

     /**
     * Validate form and make a post request
     * for spontaneous application.
     *
     * @param  Illuminate\Http\Request  $request
     * @return Response
     */
    public function spontaneous(Request $request)
    {
        $rules = [
            'category' => 'required',
            'city' => 'required',
        ];

        if ($request->get('type') === 'upload') {
            $rules = array_merge($rules, [
                'attachment' => 'mimes:doc,docx,pdf|max:2048|required',
            ]);
        }

        $validator = $this->validate($request, $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        return $this->postApi($request, 'spontaneous');
    }

    /**
     * Validate form with given rules
     *
     * @param  Illuminate\Http\Request  $request
     * @param  Array  $rules
     * @return Validator
     */
    protected function validate(Request $request, $rules)
    {
        app()->setLocale($request->get('locale'));
        $this->translation->setContext($request->get('locale'));

        $data = array_merge($rules, [
            'lastname' => 'required',
            'firstname' => 'required',
            'phone' => 'regex:/\d{3}[\-]\d{3}[\-]\d{4}/|required',
            'email' => 'email|required',
        ]);

        return Validator::make($data, $rules);
    }

    /**
     * Make a post request with given data.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  String  $postType
     * @return Response
     */
    protected function postApi(Request $request, $postType)
    {
        ['url' => $url, 'type' => $type] = $this->getRequestConfig($request->get('type'));

        // Guzzle Client
        $client = new Client([
            'base_uri' => Config::get('haleks.api::url'),
        ]);

        try {
            $response = $client->post($url, (new ApplicationTransformer)->transform($request->all(), $type));
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 422);
        }

        return response()->json($this->translation->trans("form.$type-success"), 200);
    }

    /**
     * Get the request configuration for the given form type.
     *
     * @param  String  $type
     * @return Array
     */
    protected function getRequestConfig($type)
    {
        $locale = app()->getLocale();

        if ($type === 'upload') {
            return [
                'type' => 'multipart',
                'url' => "/api/v1.0/Candidats/upload/$locale",
            ];
        }

        return [
            'type' => 'json',
            'url' => "/api/v1.0/CandidatsSpontanes/spSend/$locale",
        ];
    }
}
